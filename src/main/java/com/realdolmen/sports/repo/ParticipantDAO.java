package com.realdolmen.sports.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.realdolmen.sports.domain.Participant;
import com.realdolmen.sports.domain.ParticipantBuilder;

public class ParticipantDAO {

	private static Connection connection;
	private static final String USER_NAME = "root";
	private static final String PASS_WORD = "";
	private static final String URL = "jdbc:mysql://localhost:3306/sports";

	public static Connection initConnection(String user, String pass, String jdbcUrl) throws SQLException {
		connection = DriverManager.getConnection(jdbcUrl, user, pass);
		return connection;
	}
	
	protected List<Participant> executer(String query) {
		return this.execute(query);
	}

	
	private List<Participant> execute(String query){
		List<Participant> participants = new ArrayList<Participant>();
		try {
			if(connection == null||connection.isClosed()) {
				initConnection(USER_NAME, PASS_WORD, URL);
			}
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			while(result.next()) {
				participants.add(
						new ParticipantBuilder()
						.setId(result.getInt("id"))
						.setFirstName(result.getString("firstname"))
						.setName(result.getString("participantname"))
						.build()
						);
				
			}
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return participants;
	}

	public List<Participant> findAllParticipants() {
		return execute("SELECT * from participant");
	}

	public Participant findParticipantById(int id) {
		List<Participant> result = execute("SELECT * from participant where id="+id);
		return result.isEmpty()?null:result.get(0);
	}

}
