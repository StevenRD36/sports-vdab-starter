package com.realdolmen.sports;

import com.realdolmen.sports.repo.ParticipantDAO;
import com.realdolmen.sports.service.ParticipantService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       ParticipantService service = new ParticipantService(new ParticipantDAO());
       String partJson = service.allParticipantsToJson();
       service.writeToFile(partJson);
    }
}
