package com.realdolmen.sports.domain;

import java.time.LocalDate;

public class ParticipantBuilder {

	private Participant participant;
	
	
	public ParticipantBuilder() {
		participant = new Participant();
	}
	
	public ParticipantBuilder setId(int id) {
		this.participant.setId(id);
		return this;
	}
	
	public ParticipantBuilder setName(String name) {
		this.participant.setName(name);
		return this;
	}
	
	public ParticipantBuilder setFirstName(String name) {
		this.participant.setFirstName(name);
		return this;
	} 
	
	public ParticipantBuilder setBirthDate(LocalDate birtDate) {
		this.participant.setBirthDate(birtDate);
		return this;
	}
	
	public ParticipantBuilder setSex(char sex) {
		this.participant.setSex(sex);
		return this;
	}
	
	
	public Participant build() {
		return participant;
	}
	
}
