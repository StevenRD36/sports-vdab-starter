package com.realdolmen.sports.domain;

import java.time.LocalDate;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

public class Participant {
	/*
	 * id int primary key auto_increment, 
	 * participantname varchar(30) not null,
	 * firstname varchar(20) not null, 
	 * birthdate date not null, 
	 * sex char(1) not null, 

	 */
	
	private int id;
	private String name;
	private String firstName;
	private LocalDate birthDate;
	private char sex;
	private Address address;
	private Contact contact;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public char getSex() {
		return sex;
	}
	public void setSex(char sex) {
		this.sex = sex;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	
	
}
