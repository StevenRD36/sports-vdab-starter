package com.realdolmen.sports.repo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.realdolmen.sports.domain.Participant;

@RunWith(JUnitPlatform.class)
public class ParticipantDAOTest {

	private ParticipantDAO participantDAO;
	
	@BeforeEach
	private void init() {
		participantDAO = new ParticipantDAO();
	}
	
	@Test
	public void findByIdNotNullTest() {
		Participant result = participantDAO.findParticipantById(1);
		assertNotNull(result);
		assertEquals(result.getFirstName(),"Annick");
	}
	
	@Test
	public void findAllParticipantsTest() {
		List<Participant> result = participantDAO.findAllParticipants();
		System.out.println(result.size());
		assertFalse(result.isEmpty());
	}
	
	//Test on executer, just to show you it works on different queries
//	@Test
//	public void executerOfQueryWhereNameStartsWith() {
//		List<Participant> result = participantDAO.executer("SELECT * FROM participant WHERE participantname LIKE 'A%'");
//		assertFalse(result.isEmpty());
//		for(Participant p : result) {
//			assertTrue(p.getName().startsWith("A"));
//		}
//	}
	
}
