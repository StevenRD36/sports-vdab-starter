package com.realdolmen.sports.domain;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
public class ParticipantBuilderTest {

	private ParticipantBuilder builder;
	
	@BeforeEach
	public void init() {
		builder = new ParticipantBuilder();
	}
	
	@Test
	public void participantBuilderBuildsParticipantTest() {
		Participant result = builder.setFirstName("Steven")
		.setName("De Cock")
		.setBirthDate(LocalDate.of(1987, 1, 8))
		.setSex('M')
		.build();
		
		assertAll(
				()-> assertEquals(result.getName(),"De Cock"),
				()-> assertEquals(result.getFirstName(),"Steve"),
				()-> assertEquals(result.getBirthDate(), LocalDate.of(1987, 1, 8)),
				()-> assertEquals(result.getSex(),'M')
				);
	}
}
