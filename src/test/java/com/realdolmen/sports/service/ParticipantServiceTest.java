package com.realdolmen.sports.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.realdolmen.sports.domain.Participant;
import com.realdolmen.sports.domain.ParticipantBuilder;
import com.realdolmen.sports.repo.ParticipantDAO;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ParticipantServiceTest {

	@Mock
	private ParticipantDAO daoMock;

	@InjectMocks
	private ParticipantService participantService;

	@Test
	public void participantForIdToJsonStringReturnsCorrectTest() {

		Participant p = new ParticipantBuilder().setFirstName("Steven").setName("De Cock").build();
		when(daoMock.findParticipantById(1)).thenReturn(p);
		
		String result = participantService.participantForIdToJson(1);
		
		assertTrue(result.contains("Steven")&&result.contains("De Cock"));
		verify(daoMock,times(1)).findParticipantById(1);
	}
	
	@Test
	public void participantForIdToJsonStringNotFoundTest() {

		when(daoMock.findParticipantById(1)).thenReturn(null);
		
		String result = participantService.participantForIdToJson(1);
		
		assertEquals("not found", result.toLowerCase());
		verify(daoMock,times(1)).findParticipantById(1);
	}
	
	@Test
	public void allParticipantToJsonStringReturnsCorrectTest() {

		List<Participant> p = Arrays.asList(new ParticipantBuilder().setFirstName("Steven").setName("De Cock").build(),
				new ParticipantBuilder().setFirstName("Jarre").setName("Vereecken").build());
		when(daoMock.findAllParticipants()).thenReturn(p);
		
		String result = participantService.allParticipantsToJson();
		
		assertTrue(result.contains("Steven")&&result.contains("De Cock"));
		assertTrue(result.contains("Jarre")&&result.contains("Vereecken"));
		verify(daoMock,times(1)).findAllParticipants();
	}
	
	@Test
	public void allParticipantToJsonStringNotFoundTest() {
		when(daoMock.findAllParticipants()).thenReturn(new ArrayList<Participant>());
		
		String result = participantService.allParticipantsToJson();
		
		assertEquals("not found", result.toLowerCase());
		verify(daoMock,times(1)).findAllParticipants();
	}

}
